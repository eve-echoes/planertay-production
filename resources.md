**8MG-J9 < IPS-QB < Gemanite**

**8MG-J6 I**
- Lustering_Alloy - poor
- Dark_Compound - medium
- Supertensile_Plastics - poor
- Construction_Blocks - poor
- Plasmoids - medium

**8MG-J6 II**
- Dark_Compound - poor
- Base_Metals - medium
- Toxic_Metals - medium
- Silicate_Glass - medium
- Plasmoids - medium

**8MG-J6 III**
- Dark_Compound - medium
- Base_Metals - medium
- Toxic_Metals - medium
- Industrial_Fibers - poor
- Plasmoids - medium

**8MG-J6 IV**
- Dark_Compound - medium
- Industrial_Fibers - medium
- Supertensile_Plastics - medium
- Construction_Blocks - poor
- Plasmoids - medium

**8MG-J6 V**
- Crystal_Compund - poor
- Reactive_Gas - poor
- Reactive_Metals - medium
- Nanites - medium
- Suspended_Plasma - rich

**8MG-J6 VI**
- Condensed_Alloy - rich
- Reactive_Gas - medium
- Industrial_Fibers - medium
- Condensates - PERFECT
- Heavy_Water - PERFECT
- Liquid_Ozone - medium

**8MG-J6 VII**
- Lustering_Alloy - rich
- Glossy_Compund - poor
- Noble_Gas - medium
- Polyaramids - medium
- Coolant - PERFECT
- Ionic_Solutions - rich

**8MG-J6 VIII**
- Lustering_Alloy - rich
- Sheen_Compund - medium
- Reactive_Metals - rich
- Polyaramids - medium
- Coolant - rich
- Ionic_Solutions - poor

**8MG-J6 IX**
- Sheen_Compund - poor
- Reactive_Gas - medium
- Noble_Gas - PERFECT
- Base_Metals - rich
- Reactive_Metals - medium
- Polyaramids - poor

**8MG-J6 X**
- Lustering_Alloy - medium
- Precious_Alloy - poor
- Base_Metals - rich
- Coolant - poor
- Liquid_Ozone - medium

**8MG-J9 < IPS-QB < Gemanite**
